#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import utils
from GaussianModel import GaussianModel, GaussianMixtureModel
from sklearn import svm
from sklearn.mixture import GaussianMixture

fname = os.path.join("/home/omri/fish/training_1.csv")
training_data = pd.read_csv(fname, sep= " ")

fname2 = os.path.join("/home/omri/fish/validation.csv")
validation_data = pd.read_csv(fname2, sep= ",")
        
y_training = utils.binary_classification(training_data)
y_truth = utils.binary_classification(validation_data)


train_cols = utils.find_columns(["Distance", "Max"], training_data)
X_train = training_data[train_cols].values

validation_cols = utils.find_columns(["Distance", "Max"], validation_data)
X_validation = validation_data[validation_cols].values

# compute preprocessing parameters
preprocessing = utils.Preprocess(X_train)

Z_train = preprocessing.center_and_scale(X_train)
Z_validation = preprocessing.center_and_scale(X_validation)


model_type = 'GaussianMixture'

#train the model
if model_type == 'GaussianModel':
    model = GaussianModel()
elif model_type == 'GaussianMixture':
    n_components = len(np.unique(y_training))
    model = GaussianMixtureModel(n_components, covariance_type="full")
elif model_type == 'SVC':
    model = svm.SVC(kernel='linear', probability=True)
else:
    raise ValueError('The classifier type %s in not supported' % model_type)

model.fit(Z_train, y_training)
    
# predict the classification of the validation set
validation_predict = model.predict(Z_validation)
validation_probability = model.predict_proba(Z_validation)

# compare the prediction with the correct labels
sel_negative = y_truth == 0
n_neg = sum(sel_negative)
n_true_neg = sum(validation_predict[sel_negative] == 0)
n_false_pos = sum(validation_predict[sel_negative] == 1)
n_true_pos = sum(validation_predict[~sel_negative] == 1)
n_false_neg = sum(validation_predict[~sel_negative] == 0)

print "Found %s True Negatives" % n_true_neg
print "Found %s False Positives" % n_false_pos
print "Found %s True Positives" % n_true_pos
print "Found %s False Negatives" % n_false_neg

print "The truth table: predicted\\real"
print np.matrix([[n_true_neg, n_false_pos], [n_false_neg, n_true_pos]])


plt.figure()
plt.plot(validation_probability[sel_negative, 0], validation_probability[sel_negative, 1], "o")
plt.plot(validation_probability[~sel_negative, 0], validation_probability[~sel_negative, 1], "ro")
plt.title("Probability of the prediction")
plt.xlabel("Probability of non-seizure")
plt.ylabel("Probability of seizure")
plt.axis("square")
plt.legend(["non-seizure", "seizure"])
plt.show()

# combining the two datasets and saving it
X_combined = np.concatenate((X_train, X_validation), axis=0)
y_combined = np.concatenate((y_training, y_truth))
data_combined = pd.DataFrame(X_combined, columns=["Distance", "Maximum_Velocity"])
data_combined["Classification"] = y_combined
data_combined.to_csv("training_full.csv", sep=" ", index=False)

plt.figure()
plt.plot(X_train[y_training == 0, 0], X_train[y_training == 0, 1], "o")
plt.plot(X_train[y_training == 1, 0], X_train[y_training == 1, 1], "ro")
plt.plot(X_validation[validation_predict == 0, 0], X_validation[validation_predict == 0, 1], "b+", ms=8)
plt.plot(X_validation[validation_predict == 1, 0], X_validation[validation_predict == 1, 1], "r+", ms=8)
plt.legend(["training no-seizure", "training seizure", "predict no-seizure", "predict seizure"], loc=4)
plt.xlabel("Distance [mm]")
plt.ylabel("Maximum Velocity [mm/s]")
plt.show()

cols = utils.find_columns(["Distance", "Max"], data_combined)
X = data_combined[cols].values
y = data_combined["Classification"].values
sel = y==0

#model = svm.SVC(kernel='linear', probability=True)
model.fit(X, y)
#model = Model(X, y)
label = model.predict(X)
sel2 = label==0



plt.figure()
plt.plot(X[sel, 0], X[sel, 1], "ko")
plt.plot(X[~sel, 0], X[~sel, 1], "ro")
plt.plot(X[sel2, 0], X[sel2, 1], "k+", ms=12, lw=2)
plt.plot(X[~sel2, 0], X[~sel2, 1], "r+", ms=12, lw=2)
plt.plot(model.means[0][0], model.means[0][1], "k*",ms=12)
plt.plot(model.means[1][0], model.means[1][1], "r*",ms=12)
plt.axis("equal")
plt.show()

