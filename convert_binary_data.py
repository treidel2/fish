#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 17:14:33 2017

@author: omri
"""


import logging
import os

import click

import h5py
import numpy as np

MAX_UINT16 = 2**16 - 1

NUMBER_OF_WELLS = 96

#I also need to convert the orientation to get the wells appropriate to my genotyping. For that, 
#I use the following dictionary.
well_conversion = {0:0,1:8,2:16,3:24,4:32,5:40,6:48,7:56,8:64,9:72,10:80,11:88,12:1,13:9,14:17,
                   15:25,16:33,17:41,18:49,19:57,20:65,21:73,22:81,23:89,24:2,25:10,26:18,27:26,
                   28:34,29:42,30:50,31:58,32:66,33:74,34:82,35:90,36:3,37:11,38:19,39:27,40:35,
                   41:43,42:51,43:59,44:67,45:75,46:83,47:91,48:4,49:12,50:20,51:28,52:36,53:44,
                   54:52,55:60,56:68,57:76,58:84,59:92,60:5,61:13,62:21,63:29,64:37,65:45,66:53,
                   67:61,68:69,69:77,70:85,71:93,72:6,73:14,74:22,75:30,76:38,77:46,78:54,79:62,
                   80:70,81:78,82:86,83:94,84:7,85:15,86:23,87:31,88:39,89:47,90:55,91:63,92:71,
                   93:79,94:87,95:95}


def cart2pol(x, y):
	rho = np.sqrt(x**2 + y**2)
	theta = np.arctan2(y, x)
	return(rho, theta)

##find max and min value for each fish in order to identify well edges
#def max_min(cen_data_array):
#	maxxys = []
#	minxys = []
#	for n in xrange (0, NUMBER_OF_WELLS*2,2):
#		maxtest = np.amax(cen_data_array[:,n])
#		mintest = np.amin(cen_data_array[:,n])
#		if maxtest == mintest and maxtest == 0:
#			maxxys.append(0)
#			maxxys.append(0)
#			minxys.append(0)
#			minxys.append(0)
#			# IF WELL IS EMPTY OR NOTHING EVER MOVES NEED A CHECK - ie, if MIN AND MAX ARE EQUAL?
#		else:
#			maxrealx = maxtest
#			minrealx = np.amin(cen_data_array[:,n][np.nonzero(cen_data_array[:,n])])
#			maxrealy = np.amax(cen_data_array[:,n+1])
#			minrealy = np.amin(cen_data_array[:,n+1][np.nonzero(cen_data_array[:,n+1])])
#			maxxys.append(maxrealx)
#			maxxys.append(maxrealy)
#			minxys.append(minrealx)
#			minxys.append(minrealy)
#	maxxysnp = np.array(maxxys)
#	minxysnp = np.array(minxys)
#	return( maxxysnp, minxysnp)

#find max and min value for each fish in order to identify well edges
def max_min(cen_data_array):
    """
    cen_data_array (time_samples x NUMBER_OF_WELLS*2)
    """
    maxxys = []
    minxys = [] # FIXME: preallocate array
    for n in xrange(0, NUMBER_OF_WELLS*2, 2):
        maxtest = np.amax(cen_data_array[:, n])
#        mintest = np.amin(cen_data_array[:, n])
#        if maxtest == mintest and maxtest == 0:
        if maxtest < 1: 
            maxxys.append(0)
            maxxys.append(0)
            minxys.append(0)
            minxys.append(0)
            # IF WELL IS EMPTY OR NOTHING EVER MOVES NEED A CHECK - ie, if MIN AND MAX ARE EQUAL?
        else:
            maxrealx = maxtest
            minrealx = np.amin(cen_data_array[:, n][np.nonzero(cen_data_array[:, n])])
            maxrealy = np.amax(cen_data_array[:, n+1])
            minrealy = np.amin(cen_data_array[:, n+1][np.nonzero(cen_data_array[:, n+1])])
            maxxys.append(maxrealx)
            maxxys.append(maxrealy)
            minxys.append(minrealx)
            minxys.append(minrealy)
    maxxysnp = np.array(maxxys)
    minxysnp = np.array(minxys)
    return maxxysnp, minxysnp



#I use the following function to center the data and break up into rho, theta, x, and y.

def convert_to_polar(cen_data_array):
    """
    Args:
        cen_data_array (numpy.array): a 2D array with the coordinates the fish
    
    Returns:
        
    """
    (maxxysnp, minxysnp) = max_min(cen_data_array)
    midcoords = (maxxysnp + minxysnp) / 2
    midcoords = midcoords.astype(np.int16)
    cen_data_array = cen_data_array.astype(np.int16)
    cen_data_array[cen_data_array == 0] = -10000 # just setting to very low value to make it easier to ignore
    # subtract middle coordinate to get everything centered about 0
    zerodcoords = np.zeros(np.shape(cen_data_array))
    for i in xrange (0, NUMBER_OF_WELLS*2):
        zerodcoords[:, i] = cen_data_array[:, i] - midcoords[i]
        zerodcoords[zerodcoords < -5000 ] = 0 
        # zerodcoords currently contains negative numbers, which I think mean that the fish hasn't moved yet
        thetadata = np.zeros((len(cen_data_array), NUMBER_OF_WELLS))
        rhodata = np.zeros((len(cen_data_array), NUMBER_OF_WELLS))
        xzerod = np.zeros((len(cen_data_array), NUMBER_OF_WELLS))
        yzerod = np.zeros((len(cen_data_array), NUMBER_OF_WELLS))
        for i in xrange (0, NUMBER_OF_WELLS):
            (rhodata[:,i], thetadata[:,i]) = cart2pol(zerodcoords[:, 2*i], zerodcoords[:, 2*i+1])
            xzerod[:,i] = zerodcoords[:,2*i]
            yzerod[:,i] = zerodcoords[:,2*i+1]
    return (rhodata, thetadata, xzerod, yzerod)



centroid_file = '/home/omri/fish/testlog.centroid1'

def load_centriod_data(centroid_file, num_of_wells):  # type String -> List[numpy.array]
    """Loads a data file (centriod) and seperate the data for each well
    """
    with open(centroid_file, 'rb') as fid:
        data = np.fromfile(fid, '>u2')
    num_samples = data.size/(num_of_wells*2)
    data = data.reshape(num_samples, 2*num_of_wells)
    data[data == MAX_UINT16] = 0 # just setting to zero to make it easier 
    return [data[:, i_well:i_well+2] for i_well in xrange(0, 2*num_of_wells, 2)]


    # to ignore
    # This is because they are all values of -16 and the initial type of the array is unsigned int,
    # but it should be clear that it means the fish hasn't moved yet
    # At this point I'm not 100% sure whether or not the -1 means the fish hasn't been 
    # captured yet because it hasn't moved or whether it means something else
    # converting them to zero for now so that it makes it easier to deal with the 
    # downstream max/min tests


well_pnts =  load_centriod_data(centroid_file, num_of_wells=NUMBER_OF_WELLS)

def compute_well_center(points):
    print points.shape[0]
    x = points[:, 0]
    sel_x = x > 0
    min_x = min(x[sel_x])
    max_x = max(x)
    y = points[:, 1]
    sel_y = y > 0
    min_y = min(y[sel_y])
    max_y = max(y[sel_y])
    return [0.5*(min_x + max_x), 0.5*(min_y + max_y), 
            (max_x - min_x), (max_y - min_y)] 


import pandas as pd
import matplotlib.pyplot as plt
from multiprocessing.pool import ThreadPool

num_processes = 3
pool = ThreadPool(num_processes)
well_centers = pool.map(compute_well_center, well_pnts)
well_centers = pd.DataFrame(well_centers, columns = ['cx', 'cy', 'lx', 'ly'])


well_data_path = '/home/omri/fish/well_data.h5'
with h5py.File(well_data_path, 'w') as h5:
    for i in xrange(NUMBER_OF_WELLS):
        print i
        grp = h5.create_group('well_%s' % i)
        grp.create_dataset('center', data=well_centers.loc[i, ['cx', 'cy']])
        grp.create_dataset('cartesian', data=well_pnts[i])
        

with h5py.File(well_data_path, 'r+') as h5:
    for i in xrange(NUMBER_OF_WELLS):
        print i
        grp_name = 'well_%s' % i
        well_xy = h5['{}/cartesian'.format(grp_name)][()].astype(np.int16)
        invalid_sel = well_xy < 1
        midpoint = h5['{}/center'.format(grp_name)][()].astype(np.int16)
        well_xy -= midpoint
        well_xy[invalid_sel] = 0
        h5[grp_name].create_dataset(name='centered_xy', data=well_xy) 
        position_diff = np.diff(well_xy.astype(float), axis=0)
        distance = np.sqrt(np.sum(position_diff**2, axis=1))
        h5[grp_name].create_dataset(name='distance', data=distance)
        

date = '27/12/2016'
samping_rate = 27 # frames per second
dt = int(round(1.0/samping_rate * 1e3)) # in milliseconds
resampling_time = 5 # in seconds
with h5py.File(well_data_path, 'r+') as h5:
   for i in xrange(NUMBER_OF_WELLS):
        print i
        grp_name = 'well_%s' % i
        distance = h5['{}/distance'.format(grp_name)][()]
        distance = pd.Series(index=pd.date_range(date, periods=len(distance), freq='{}ms'.format(dt)), 
                             data=distance)    
        binned_distance = distance.resample('{}s'.format(resampling_time)).sum()
        binned_max = distance.resample('{}s'.format(resampling_time)).max()
        h5['{}/binned_distance_sum'.format(grp_name)] = binned_distance
        h5['{}/binned_distance_max'.format(grp_name)] = binned_max


        
plt.figure()
plt.errorbar(well_centers.cx, well_centers.cy, 
             well_centers.lx/2, well_centers.ly/2, "o")
plt.axis('equal')
plt.show()
