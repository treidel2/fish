#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 17:14:33 2017

@author: omri
"""

import logging
import enum
import os
import time

import click
import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from multiprocessing.pool import ThreadPool


MAX_UINT16 = 2**16 - 1


class LogLevel(enum.Enum):
    debug = logging.DEBUG
    info = logging.INFO
    warn = logging.WARN
    error = logging.ERROR


def load_centriod_data(centroid_file, num_of_wells):  # type String -> List[numpy.array]
    """Loads a data file (centriod) and seperate the data for each well
    
    Args:
        centroid_file (str): binary file containing the fish centriod position 
        num_of_wells (int): number of wells
    
    Returns:
        list[numpy.array]: list of XY point per well
    """
#    TODO: in the future the file size might be too big to load in to RAM. 
#          Consider loading it in chunks and save it in the HDF5 file 
    with open(centroid_file, 'rb') as fid:
        t_0 = time.time()
        logging.info("Loadting data from %s ...", centroid_file)
        data = np.fromfile(fid, '>u2')
        loading_time = time.time() - t_0 
    logging.info("    ... Done in %s seconds", loading_time)
    num_samples = data.size/(num_of_wells*2)
    data = data.reshape(num_samples, 2*num_of_wells)
    data[data == MAX_UINT16] = 0 # just setting to zero to make it easier 
    return [data[:, i_well:i_well+2] for i_well in xrange(0, 2*num_of_wells, 2)]


def compute_well_center(points, num_samples=None):
    """ Find the midpoint and width of the set of points
    
    Args:
        points: numpy array Nx2 array of xy points
        
    Retunrs:
        list: midpoint_x, midpoint_y, width_x, width_y
    """
    num_samples = int(1e5)
    if num_samples is not None:
        inds = np.random.choice(np.arange(points.shape[0]), num_samples, replace=False)
        points = points[inds, :] 
        
    x = points[:, 0]
    sel_x = x > 0
    min_x = min(x[sel_x])
    max_x = max(x)
    y = points[:, 1]
    sel_y = y > 0
    min_y = min(y[sel_y])
    max_y = max(y[sel_y])
    return [0.5*(min_x + max_x), 0.5*(min_y + max_y), 
            (max_x - min_x), (max_y - min_y)] 


def well_positions(data_pnts, num_processes=3):
    """ Computes the well positions using multiprocessing
    
    Args:
        data_pnts (list[numpy.array]): list of points for each well 
        num_processes: number of processes to use
    
    Returns:
        pandas Dataframe: the center and width of each well
    """    
    pool = ThreadPool(num_processes)
    logging.info("Using %s processes to compute well centers", num_processes)
    t_0 = time.time()
    well_centers = pool.map(compute_well_center, data_pnts)
    logging.debug("Finished computing well_centers after %s seconds", (time.time() - t_0))
    well_centers = pd.DataFrame(well_centers, columns = ['cx', 'cy', 'lx', 'ly'])
    return well_centers


def create_well_position_image(well_centers, num_wells, well_pos_filename):
    """ Plot the well centers with the well internal number
    
    Args:
        well_centers (pandas.Datafreme): the well centers 
        num_wells (int): number of wells 
        well_pos_filename (str): filename for the image
    """
    for i in range(num_wells):
        x = well_centers.loc[i, 'cx']
        y = well_centers.loc[i, 'cy']
        plt.plot(x, y, "o")
        plt.annotate(str(i), xy=(x+5, y+5))
    logging.info("Saving well mapping image in %s", well_pos_filename)
    plt.savefig(well_pos_filename)


def _create_output_group(group):
    number = group['well_number'][()]
    timestamp = pd.to_datetime(group['timestamp'][()])
    binned_distance = group['binned_distance_sum'][()]
    binned_max_speed = group['binned_max_velocity'][()] 

    out_df = pd.DataFrame(dict(timestamp=timestamp,
                               distance=binned_distance,
                               max_velocity=binned_max_speed))
    out_df['well_number'] = number
    out_df.set_index('timestamp', inplace=True)
    return out_df[['Total_distance', 'Maximum_velocity', 'well_number']] 


def write_csv(hdf5_filename, csv_filename):
    logging.info("Writing results to %s" % csv_filename)
    with h5py.File(hdf5_filename, 'r') as h5:
        for i, group_name in enumerate(h5.iterkeys()):
            header = False
            if i == 0:
                header = True 
            data = _create_output_group(h5[group_name])
            data.to_csv(csv_filename, mode='a', header=header)


def _preprocess(centroid_file, num_of_wells, outfile, well_pos_filename, samping_rate, 
                resampling_period, start_time, pixel_size, num_processes):
    """ preprocess the centriod data before classification.
    
    Args:
        centroid_file (str): path to centriod file 
        num_of_wells (int): number of wells 
        outfile (str): path for output file (CSV) 
        well_pos_filename (str): file name for the well position image 
        sampling_rate (float): number of time samples in a second
        resampling_period (float): period of the resempling (in seconds)
        start_time (str): data collection start date i.e '2016-27-12 13:47:52'
        pixel_size: size of each pixel (in milimeters)
        num_processes (int): number of processes

    Returns:
        well_data_path (str): path to HDF5 file
    """
    #centroid_file = '/home/omri/fish/testlog.centroid1'
    well_pnts =  load_centriod_data(centroid_file, num_of_wells=num_of_wells)
    well_centers = well_positions(well_pnts, num_processes)
    create_well_position_image(well_centers, num_of_wells, well_pos_filename)
    well_data_path = outfile + '_tmp.h5'

    # write points and centers for each well 
    # well_data_path = '/home/omri/fish/well_data.h5'
    with h5py.File(well_data_path, 'w') as h5:
        logging.info("Saving the %s wells center data in the HDF5 file %s", 
                     num_of_wells, well_data_path)
        for i in xrange(num_of_wells):
            grp = h5.create_group('well_%s' % i)
            grp.create_dataset('well_number', data=i)
            grp.create_dataset('center', data=well_centers.loc[i, ['cx', 'cy']])
            grp.create_dataset('cartesian', data=well_pnts[i])
#    clear the memory since we can now load everything from disk
    del well_pnts
    
    # compute eucledian distance between consecutive points
    with h5py.File(well_data_path, 'r+') as h5:
        logging.debug("Computing distance between consecutive time samples")
        for i in xrange(num_of_wells):
            grp_name = 'well_%s' % i
            well_xy = h5['{}/cartesian'.format(grp_name)][()].astype(np.int16)
            invalid_sel = well_xy < 1
            midpoint = h5['{}/center'.format(grp_name)][()].astype(np.int16)
            well_xy -= midpoint
            well_xy[invalid_sel] = 0
            h5[grp_name].create_dataset(name='centered_xy', data=well_xy) 
            position_diff = np.diff(well_xy.astype(float), axis=0)
            distance_pixels = np.sqrt(np.sum(position_diff**2, axis=1))
            h5[grp_name].create_dataset(name='distance_pixels', data=distance_pixels)

    dt = int(round(1.0/samping_rate * 1e3)) # in milliseconds
    with h5py.File(well_data_path, 'r+') as h5:
        logging.debug("Resampling data and computing total distance and maximum velocity")
        for i in xrange(num_of_wells):
            grp_name = 'well_%s' % i
            distance = h5['{}/distance_pixels'.format(grp_name)][()].astype(float)*pixel_size
            distance = pd.Series(index=pd.date_range(start_time, periods=len(distance),
                                                     freq='{}ms'.format(dt)), data=distance)
            speed = distance*samping_rate # mm/s
            binned_distance = distance.resample('{}s'.format(resampling_period)).sum()
            binned_max_speed = speed.resample('{}s'.format(resampling_period)).max()
                
            h5['{}/timestamp'.format(grp_name)] = map(str, binned_distance.index)
            h5['{}/binned_distance_sum'.format(grp_name)] = binned_distance
            h5['{}/binned_max_velocity'.format(grp_name)] = binned_max_speed

    write_csv(well_data_path, outfile)
    return well_data_path


@click.command()
@click.argument('centroid-file', type=click.Path(exists=True))
@click.argument('number-wells', type=int)
@click.argument('output-file', type=click.Path(exists=False))
@click.argument('sampling-rate', type=float)
@click.argument('pixel-size', type=float)
@click.argument('start-time', type=str)
@click.option('--resampling-period', type=int, default=5, 
                help='period of the resempling in seconds [default=5.0 seconds]')
@click.option('--number-processes', type=int, default=3, 
                help='number of paraell processes touse [default=3]')
@click.option('--loglevel', type=click.Choice(['debug', 'info', 'warn', 'error']), 
              default='info', help='logging level [default=info]', )
def preprocess(centroid_file, number_wells, output_file, sampling_rate, 
                resampling_period, start_time, pixel_size, number_processes, loglevel):
    """Prepocess the centriod file 
    
    CENTRIOD_FILE: a path to a binary file containing the fish centriod data
    
    NUMBER_WELLS: number of wells 
    
    OUTPUT_FILE: a path to a CSV file with the preprocessed data
    
    SAMPLING_RATE: number of samples per second
    
    PIXEL_SIZE: size of each pixel in milimeters

    START_TIME: start time of data collection start date e.g '2018-01-21 13:47:12'. DON'T forget the quotes!
    
    Example Usage: python ./preprocess.py testlog.centroid1 96 test_output 27.0 0.2 '2018-01-21 13:47:12'
    """
    
    logging.basicConfig(level=getattr(LogLevel, loglevel).value)

    well_pos_filename = output_file + '_well_position.PNG'
    hdf5_file = _preprocess(centroid_file, number_wells, output_file, well_pos_filename,
                            sampling_rate, resampling_period, start_time, pixel_size,
                            number_processes)

    if loglevel != 'debug':
        try:
            os.remove(hdf5_file)
        except OSError:
            logging.info("Could not remove file %s" % hdf5_file)


if __name__ == '__main__':
    preprocess()

