#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt

import utils
import classifier


import numpy as np
from scipy import linalg
import matplotlib as mpl
import itertools

color_iter = itertools.cycle(['red', 'c', 'cornflowerblue', 'gold', 'darkorange'])

def plot_results(X, Y_, means, covariances, index, title):
    splot = plt.subplot(1, 1, 1 + index)
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.plot(X[Y_ == i, 0], X[Y_ == i, 1], ".", color=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)
    plt.title(title)
    plt.axis("equal")


def svc_boundary(svc):
    w = svc.coef_[0] # the normal to the decision boundary
    a = -w[0] / w[1]
    b = -(svc.intercept_[0]) / w[1]
    margin = 1 / np.sqrt(np.sum(svc.coef_ ** 2))
    return a, b, margin


def plot_svc(X, svc, y):
    """
    
    """
    h = .01  # step size in the mesh
    # create a mesh to plot in (this is the background colors that represent the classes)
    x_min, x_max = X[:, 0].min() - 0.6, X[:, 0].max() + 0.6
    y_min, y_max = X[:, 1].min() - 0.6, X[:, 1].max() + 0.6
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    
    Z = svc.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)

    # plot the decision boundary and margin
    a, b, margin = svc_boundary(svc)
    boundary_x = np.linspace(-1, 1)
    boundary_y = a * boundary_x + b
    yy_down = boundary_y + a * margin
    yy_up = boundary_y - a * margin

    # Plot also the training points
    plt.plot()
    # plot the points
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
    # plot the decision boundary and margin
    plt.plot(boundary_x, boundary_y, 'w')
    plt.plot(boundary_x, yy_down, 'k--')
    plt.plot(boundary_x, yy_up, 'k--')

    plt.xlabel('Distance')
    plt.ylabel('Maximum_Velocity')
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
#    plt.xticks(())
#    plt.yticks(())

    
#print (model.predict(X_train) == y_train).sum()/float(len(y_train))

#data = pd.read_csv("out_GM_full.csv", sep=",")
#c = data.Classification.values
#p0 = data.Probability_0.values
#p1 = data.Probability_1.values

#cols = utils.find_columns(["Distance", "Maximum"], data)
#X = preprocessing.center_and_scale(data[cols].values)
#sel = np.prod(np.isfinite(X),axis=1).astype(bool)
#plot_results(X[sel, :], model.predict(X[sel, :]), model.means, model.covariances, 0, "foo")


# JEREMY: make sure svc_penalty has the same value as the one you've used for the classification
model, preprocessing = classifier._train("training_full.csv", "SVC", svc_penalty=100)


traning_data = pd.read_csv('training_full.csv', sep=' ')
X_train = traning_data[['Distance', 'Maximum_Velocity']].values
X_train = preprocessing.center_and_scale(X_train)
y_train = traning_data.Classification.values
    
plot_svc(X_train, model, y_train)

a,b,margin = svc_boundary(model)

n = np.linalg.norm(model.coef_[0])
v = model.coef_[0]/n
#sel = np.abs(d) <= 0.2
#plt.plot(X_train[sel, 0], X_train[sel, 1], "kx")


data = pd.read_csv("20170613_SI_5mM_noAC_out_classified.csv")
cols = utils.find_columns(['Distance', 'Maximum'], data)

X_ = preprocessing.center_and_scale(data[cols].values)
X = X_ - np.array([0, b])
d = np.squeeze(np.dot(X, v))
sel = np.abs(d) <= 1*margin

sel_p = (d <= 1*margin) & (d > 0)
sel_n = (d > -1*margin) & (d < 0)

#plt.plot(X_[sel_p, 0], X_[sel_p, 1], "k.")
#plt.plot(X_[sel_n, 0], X_[sel_n, 1], "y.")

sel_1 = data.loc[sel, 'Classification'] == 1
sel_0 = data.loc[sel, 'Classification'] == 0

plt.plot(X_[sel, 0][sel_0], X_[sel, 1][sel_0], "k.")
plt.plot(X_[sel, 0][sel_1], X_[sel, 1][sel_1], "y.")

suspects = data[sel]
suspects.to_csv("borderline_cases.csv")


#from sklearn.model_selection import cross_val_score, cross_val_predict
#from sklearn import svm
#plt.figure()
#for c in [50, 100, 200]:
#
#    model = svm.SVC(kernel='linear', probability=False, C=c)
#    predicted_linear = cross_val_score(model, X_train, y_train, cv=10)    
#    plt.hist(predicted_linear, bins=np.arange(0.5, 1.1, 0.1), histtype="step")
#

