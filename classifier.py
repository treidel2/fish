#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import logging
import os

import click
import numpy as np
import pandas as pd
import xlsx2csv

import utils


def reject_consequtives_ones(classification, threshold=3):
    """ Mark consequtive sizures > than the threshold as False posivives
    
    Args:
        classification (numpy.array): a vector of classification flags (expecting 1s and 0s)
    
    Keyword Args:
        threshold (int): number of consequtive 1s to mark for rejection
        
    Returns:
        numpy.array: new classification vector with consequtive ones are modified to 2s
    """    
    _classification = np.pad(classification, 1, mode='constant')
    _classification_diff = np.diff(_classification)
    first_1 = np.where(_classification_diff == 1)[0]
    last_1 = np.where(_classification_diff == -1)[0]
    new_classification = classification.copy() 
    sel = last_1 - first_1 >= threshold
    for f, l in zip(first_1[sel], last_1[sel]):
        new_classification[f:l] = 2
    return new_classification

    
def _train(training_file, model_type, svc_penalty):
    """ Train a Gaussian mixture model
    
    Args:
        training_file (str): A path to a CSV filae containing a labeld training set
    
    Returns:
        sklearn.mixture.GaussianMixture: a trained model
        utils.Preprocess: an object for preprocessing the data
    """
    training_data = pd.read_csv(training_file, sep=" ")
    
    X_train = training_data[["Distance", "Maximum_Velocity"]].values
    y_train = training_data.Classification.values.astype(int)
       
    preprocess = utils.Preprocess(X_train)
    X_train = preprocess.center_and_scale(X_train)
    if model_type == 'GaussianModel':
        from GaussianModel import GaussianModel
        model = GaussianModel()
    elif model_type == 'SVC':
        from sklearn import svm
        model = svm.SVC(kernel='linear', probability=True, C=svc_penalty)
    elif 'GaussianMixture' in model_type:
        from GaussianModel import GaussianMixtureModel
        cov_type = model_type.split(":")[-1]
        n_components = len(np.unique(y_train))
        model = GaussianMixtureModel(n_components, covariance_type=cov_type)
    else:
        raise ValueError('The classifier type %s in not supported' % model_type)

    model.fit(X_train, y_train)
    return model, preprocess
    

def _classify(input_file, output_file, model, preprocess, rejection_threshold):
    """Classify the data and write the classification to the same file
    
    Args:
        input_file (str): a path to a file with unclassified data
        output_file (str): a path to an output file with classified data
        model (sklearn.mixture.GaussianMixture): a trained model
        utils.Preprocess: an object for preprocessing the data
    """
    data = pd.read_csv(input_file, sep= ",", na_values='-')
    cols = utils.find_columns(["Distance", "Maximum"], data)
    X = data[cols].values.astype(float)
    sel = np.all(np.isfinite(X), axis=1)
    if sum(sel) < 1:
        logging.info("No valid entries in file %s", input_file)
        exit(0)
    
    X = X[sel, :]
    X = preprocess.center_and_scale(X)
    data.loc[sel, "Classification"] = model.predict(X)
    probabilities = model.predict_proba(X)
    data.loc[sel, "Probability_0"] = probabilities[:, 0]
    data.loc[sel, "Probability_1"] = probabilities[:, 1]
    
    data.loc[sel, "Classification"] = reject_consequtives_ones(data.loc[sel, "Classification"], 
            threshold=rejection_threshold)
    data.to_csv(output_file, sep=",", index=False)
    
    
@click.command()
@click.argument('input-file', type=click.Path(exists=True))
@click.argument('output-file', type=click.Path(exists=False))
@click.argument('training-file', type=click.Path(exists=True))
@click.option('--classifier',  type=click.Choice(['GaussianModel', 'GaussianMixture:full', 'GaussianMixture:tied', 'SVC']), 
              default="GaussianModel", help="Type of the classifier to use. Default is GaussianModel")
@click.option('--svc-penalty', type=float, default=10.0, help="SVC penalty parameter [default=10.0]")
@click.option('--rejection-threshold', type=int, default=3, help="Threshold for rejecting consequtive sizures [default=3]")
def classify(input_file, output_file, training_file, classifier, svc_penalty, rejection_threshold):
    """Classify the data and write the classification to the same file
    
    INPUT_FILE: a path to a EXCEL/CSV file with unclassified data
    
    OUTPUT_FILE: a path to a CSV file with the classified data
    
    TRAINGING_FILE: A path to a CSV filae containing a labeld training set

    Example Usage: python classifier.py classify_me.xlsx classified_scv.xlsx training_set.csv --classifier SVC

    Example Usage: python classifier.py classify_me.xlsx classified_GM_full.xlsx training_set.csv --classifier GaussianMixture:full
    """
    model, preprocess = _train(training_file, classifier, svc_penalty)
    if input_file.lower().endswith("xlsx"):
        datafile = os.path.splitext(input_file)[0] + ".csv"
        xlsx2csv.Xlsx2csv(input_file).convert(datafile)
        _classify(datafile, output_file, model, preprocess, rejection_threshold)
        os.remove(datafile)
    else:
        _classify(input_file, output_file, model, preprocess, rejection_threshold)
    
    
if __name__ == '__main__':
    classify()