#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np

def binary_classification(data):   
    """read the labels YES/NO and converts it to 1/0 
    
    Args:
        data (pandas.DataFrame): input data
        
    Returns:
        numpy.array: labels
    """
    
    if not ("label" in data.columns):
        raise Exception("Canno't find a 'label' column")
    
    labels = np.zeros(len(data))
    for i, v in enumerate(data.label):
        if v =="NO":
            labels[i] = 0
        elif v =="YES":
            labels[i] = 1
        else:
            raise Exception("Unecpexted value %s in 'label' column. Expected YES/NO only" % v)
    return labels


class Preprocess(object):
    
    def __init__(self, X_training):
        self._max_X = np.max(X_training, axis=0)
        self._min_X = np.min(X_training, axis=0)
        self._center = 0.5*(self._min_X + self._max_X)
        self._range  = self._max_X - self._min_X
    
    def center(self, X):
        return  X - self._center
        
    def scale(self, X):
        return X/self._range

    def center_and_scale(self, X):
        return self.scale(self.center(X))

        
def _find_column(keyword, data):        
    sel = [keyword in c for c in data.columns]
    if sum(sel) == 0:
        raise Exception("Cannot find a column name with the keyword '%s' " % keyword)
    if sum(sel) > 1:
        raise Exception("There is more than 1 column name with the keyword '%s: %s' " % (keyword, list(data.columns[sel])))    
    return list(data.columns[sel])

    
def find_columns(keywords, data):
    column_names = [] 
    for k in keywords:
        column_names += _find_column(k, data)
    return column_names
  
