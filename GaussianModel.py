#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np

from scipy.stats import multivariate_normal
from sklearn.mixture import GaussianMixture


class NotEnoughPointsError(Exception):
    pass


class Gaussian(object):
    """ A convinience class for a multivariate gaussian """
    def __init__(self, X):
        self._N, self._dim = X.shape
        if self._N < 2:
            raise NotEnoughPointsError("Only %s input points" % self._N)
                
        self.mean_ = X.mean(axis=0)
        self.cov_ = 1.0/(self._N-1)*np.mat(X - self.mean_).T * np.mat(X - self.mean_)
        self.precision_ = np.linalg.pinv(self.cov_)
        self.pdf_ = multivariate_normal(self.mean_, self.cov_).pdf
    
    @property
    def mean(self):
        return self.mean_
    
    @property
    def cov(self):
        return self.cov_
    
    def pdf(self, X):
        return self.pdf_(X)
        

class GaussianModel(object):
    """Model each class as a multivariate gaussian
    
    Usage:
        model = GaussianModel(X_training, y_training)
        prediction_probability = model.probabilities(X_predict)
        prediction_labels = model.predict(X_predict)
    """

    def fit(self, X, y):
        """ Fit a multivariate gaussian to each class 
        
        Args:
             X (np.array(float)): An N_points x N_features features matrix
             y (np.array(int or bool)): classification of the training set
        
        """
        self.labels = np.unique(y)
        self.n_class = len(self.labels)
        self.modes = [Gaussian(X[y==l, :]) for l in self.labels]
        
    def predict_proba(self, X):
        """ Compute class probabilities for each data point
        
        Args:
            X (np.array(float)): An N_points x N_features features matrix
        
        Returns:
            np.array(float): An N_points x N_features matrix of probability for each class
        """
        N, dim = X.shape
        P = np.zeros((N, self.n_class))
        for i, g in enumerate(self.modes):
            P[:, i] = g.pdf(X)
        
        S = P.sum(axis=1)
        for j in xrange(self.n_class):
            P[:, j] /= S
        return P
        
    def predict(self, X):
        """ Compute class probabilities for each data point
        
        Args:
            X (np.array(float)): An N_points x N_features features matrix
        
        Returns:
            np.array(bool): An N_points vector with the label of the most probable class
        """
        p = self.predict_proba(X)
        inds = np.argmax(p, axis=1)
        return self.labels[inds]

        
class GaussianMixtureModel(object):
    """ Treat all points as one multimodal model """
    def __init__(self, n_components, covariance_type):
        self._n_components = n_components
        self._model = GaussianMixture(self._n_components, covariance_type=covariance_type)
        
    def fit(self, X, y):
        """ Fit a gaussian mixure model and asign eack gaussian a label based on the training set
        
         Args:
             X (np.array(float)): An N_points x N_features features matrix
             y (np.array(int or bool)): classification of the training set
        """
        self._labels = np.unique(y)
        self._model.fit(X)
        
        # create a permutation of the means and covariances
        perm = np.zeros(self._n_components, int) 
        for i, l in enumerate(self._labels):
            sel = y==l
            current_mean = X[sel, :].mean(axis=0)
            diff_mean = self._model.means_ - current_mean 
            ind_mean = np.argmin(np.sum(diff_mean**2,axis=1))
            perm[i] = ind_mean
        self._model.means_ = self._model.means_[perm]
        self._model.covariances_= self._model.covariances_[perm]
        self._model.weights_ = self._model.weights_[perm] 
            
    @property
    def means(self):
        return self._model.means_
    
    @property
    def covariances(self):
        return self._model.covariances_
    
    @property
    def weights(self):
        return self._model.weights_

    @property
    def labels(self):
        return self._labels
        
    def predict(self, X):
        """ Compute class probabilities for each data point
        
        Args:
            X (np.array(float)): An N_points x N_features features matrix
        
        Returns:
            np.array(bool): An N_points vector with the label of the most probable class
        """
        p = self.predict_proba(X)
        inds = np.argmax(p, axis=1)
        return self.labels[inds]
    
    def predict_proba(self, X):
        P = np.zeros_like(X)
        for i in xrange(self._n_components):
            P[:, i] = self.weights[i]*multivariate_normal(self.means[i], self.covariances[i]).pdf(X)
        
        S = P.sum(axis=1)
        for j in xrange(self._n_components):
            P[:, j] /= S
        return P        